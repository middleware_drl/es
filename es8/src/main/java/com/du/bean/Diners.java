package com.du.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 杜瑞龙
 * @date 2023/4/20 9:58
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Diners implements Serializable {
    //主键Id
    public Integer id;
    //用户名
    private String username;
    //昵称
    private String nickname;
    //头像
    private String avatarUrl;
    //注册时间
    private String createDate;

}