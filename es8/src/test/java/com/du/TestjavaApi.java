package com.du;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.du.bean.Diners;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TestjavaApi {

    static RestClient restClient;
    static ElasticsearchClient elasticsearchClient;
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @BeforeAll
    public static void init() {

        //创建客户端
        RestClientBuilder restClientBuilder = RestClient.builder(
                new HttpHost("82.157.62.33", 9200)
//                new HttpHost("172.16.55.185", 9201),
//                new HttpHost("172.16.55.185", 9202)
        );

        //用户名与密码
//        CredentialsProvider credentialsProvider= new BasicCredentialsProvider();
//        credentialsProvider.setCredentials(AuthScope.ANY,
//                new UsernamePasswordCredentials("elastic","elastic"));
//        restClientBuilder.setHttpClientConfigCallback(httpClientBuilder ->
//                httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));

        restClientBuilder.setFailureListener(new RestClient.FailureListener() {
            @Override
            public void onFailure(Node node) {
                System.out.println("出错 -> " + node);
            }
        });

        restClient = restClientBuilder.build();

        //使用Jackson解析JS0N进行传输
        RestClientTransport restClientTransport = new RestClientTransport(restClient, new JacksonJsonpMapper());
        elasticsearchClient = new ElasticsearchClient(restClientTransport);
    }

    @AfterAll
    public static void close() throws Exception {
        // 跟随应用的关闭而关闭
        restClient.close();
    }

    /**
     * 创建索引
     * @throws IOException
     */
    @Test
    public void createIndex() throws IOException {

        Diners diners = new Diners();
        diners.setId(1);
        diners.setUsername("dd");
        diners.setAvatarUrl("http:www.baidu.com");
        diners.setCreateDate( LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        IndexResponse index = elasticsearchClient.index(b ->
                b.index("diners")
                        .id("1").document(diners)
        );
    }
    @Test
    public void setRestClientearch() throws IOException {

        BulkRequest.Builder builder = new BulkRequest.Builder();
        for (int i = 0; i <100 ; i++) {
            Diners diners = new Diners();
            diners.setId(1+i);
            diners.setUsername("dd"+i);
            diners.setAvatarUrl("http:www.baidu.com");
            diners.setCreateDate( LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }

    }




}
