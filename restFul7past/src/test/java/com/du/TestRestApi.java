package com.du;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestRestApi {

   static private RestClient restClient;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @BeforeAll
    public static void init(){

        RestClientBuilder restClientBuilder = RestClient.builder(
                new HttpHost("82.157.62.33", 9200)
//                new HttpHost("172.16.55.185", 9201),
//                new HttpHost("172.16.55.185", 9202)
        );

        restClientBuilder.setFailureListener(new RestClient.FailureListener(){
            @Override
            public void onFailure(Node node) {
                System.out.println("出错 -> " + node);
            }
        });

        restClient = restClientBuilder.build();
    }

    @AfterAll
    public static void close() throws Exception{
        // 跟随应用的关闭而关闭
        restClient.close();
    }

    /**
     * 查询服务器状态
     */
    @Test
    public void testInfo() throws IOException {
        Request request = new Request("GET", "/_cluster/state");
        request.addParameter("pretty", "true");
        Response response = restClient.performRequest(request);

        System.out.println("请求完成 -> "+response.getStatusLine());
        System.out.println(EntityUtils.toString(response.getEntity()));

    }

    /**
     * 创建数据
     */
    @Test
    public void testSave() throws Exception{
        Request request = new Request("POST", "/haoke/_doc/1");
        request.addParameter("pretty", "true");

        // 构造数据
        Map<String, Object> data = new HashMap<>();
        data.put("id", 2001);
        data.put("title", "南京西路 一室一厅");
        data.put("price", 3500);

        String json = MAPPER.writeValueAsString(data);
        request.setJsonEntity(json);

        Response response = restClient.performRequest(request);
        System.out.println("请求完成 -> "+response.getStatusLine());
        System.out.println(EntityUtils.toString(response.getEntity()));

    }

    /**
     * 根据id查询数据
     */
    @Test
    public void testQueryData() throws IOException {
        Request request = new Request("GET", "/haoke/_doc/1");
        request.addParameter("pretty", "true");

        Response response = restClient.performRequest(request);

        System.out.println(response.getStatusLine());
        System.out.println(EntityUtils.toString(response.getEntity()));
    }

    /**
     * 搜索数据
     */
    @Test
    public void testSearchData() throws IOException {
        Request request = new Request("POST", "/haoke/_search");
        String searchJson = "{\"query\": {\"match\": {\"title\": \"拎包入住\"}}}";
        request.setJsonEntity(searchJson);
        request.addParameter("pretty","true");
        Response response = restClient.performRequest(request);
        System.out.println(response.getStatusLine());
        System.out.println(EntityUtils.toString(response.getEntity()));
    }

}
