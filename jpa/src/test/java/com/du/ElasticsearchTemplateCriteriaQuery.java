package com.du;

import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.du.bean.Book;
import com.du.bean.Stu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.QueryBuilders;
import org.springframework.data.elasticsearch.client.erhlc.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.HighlightQuery;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.core.query.highlight.Highlight;
import org.springframework.data.elasticsearch.core.query.highlight.HighlightCommonParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2023/4/20 16:46
 */
@SpringBootTest
public class ElasticsearchTemplateCriteriaQuery {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 面向对象复杂场景下的查询
     */
    @Test
    public void cre(){

        CriteriaQuery criteriaQuery = new CriteriaQuery(
                new Criteria("name").is("solr")
                        .and("price").between(200, 1000)
        );
        PageRequest of = PageRequest.of(0, 10);
        criteriaQuery.setPageable(of);
        SearchHits<Book> search = elasticsearchTemplate.search(criteriaQuery, Book.class);

        List<Book> collect = search.get().map(SearchHit::getContent).toList();
        System.out.println();

    }

    /**
     *
     * 子查询
     */
    public  void sub(){
        Criteria miller = new Criteria("lastName").is("Miller")
                .subCriteria(
                        new Criteria().or("firstName").is("John")
                                .or("firstName").is("Jack")
                );
        Query query = new CriteriaQuery(miller);
        SearchHits<Book> search = elasticsearchTemplate.search(query, Book.class);
    }


}