package com.du;


import com.du.bean.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ElasticsearchOperationsDemo {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    ElasticsearchOperations elasticsearchOperations;

    /**
     * 保存单条
     */
    @Test
    public void save() {
        User user = new User();
        user.setId(1001L);
        user.setName("张三");
        user.setAge(20);
        user.setHobby("足球、篮球、听音乐");

        IndexQuery indexQuery = new IndexQueryBuilder()
                .withObject(user).build();

        User save = elasticsearchOperations.save(user);

        System.out.println(save);
    }

    /**
     * 批量操作
     */
    @Test
    public void testBulk() {
        List<IndexQuery> list = new ArrayList<IndexQuery>();
        for (int i = 0; i < 5000; i++) {
            User user = new User();
            user.setId(1001L + i);
            user.setAge(i % 50 + 10);
            user.setName("张三" + i);
            user.setHobby("足球、篮球、听音乐");
            IndexQuery indexQuery = new
                    IndexQueryBuilder().withObject(user).build();
            list.add(indexQuery);
        }
        elasticsearchOperations.bulkIndex(list, User.class);



    }

    /**
     * 更新
     */
    @Test
    public void testUpdate() {
        User user = new User();
        user.setId(1001L);
        user.setName("张三");
        user.setHobby("足球、篮球、听音乐");
        UpdateResponse update = elasticsearchOperations.update(user);

        System.out.println("update");
    }

    /**
     * 删除
     */
    @Test
    public void testDelete() {
        User user = new User();
        user.setId(1001L);


    }


}
