package com.du;

import com.du.bean.Book;
import com.du.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author 杜瑞龙
 * @date 2023/4/19 10:02
 */
@SpringBootTest
public class JpaTest {

    @Autowired
    BookRepository bookRepository;

    /**
     * 新增
     */
    @Test
    public void save(){
        Book book = new Book();
        book.setName("西游记");
        book.setPrice(33);
        book.setSummary("dddd");
        bookRepository.save(book);
        System.out.println();
    }

    /**
     * 批量新增
     */
    @Test
    public void saveBatch(){
        ArrayList<Book> books = new ArrayList<>();

        for (int i = 0; i <69 ; i++) {
            Book book = new Book();
            book.setId(UUID.randomUUID().toString());
            book.setName("西游记" + i);
            book.setPrice(33 + i);
            book.setSummary(  "dddd" + i);
            books.add(book);
        }
        bookRepository.saveAll(books);
    }

    /**
     * 查询所有
     */
    @Test
    public void findAll(){
        List<Book> all = bookRepository.findAll();
        System.out.println();
    }

    /**
     * 查询所有 分页
     */
    @Test
    public void findAllPage(){
        PageRequest of = PageRequest.of(0, 10);
        Page<Book> all = bookRepository.findAll(of);
        System.out.println();
    }

   @Test
    public void findByCondition(){

       List<Book> books = bookRepository.findByNameAndPrice("西游记", 33);
       System.out.println();
    }

}
