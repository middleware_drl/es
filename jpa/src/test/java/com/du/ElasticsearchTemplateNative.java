package com.du;



import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MultiMatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;
import com.du.bean.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.client.elc.NativeQueryBuilder;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2023/4/20 16:46
 */
@SpringBootTest
public class ElasticsearchTemplateNative {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;


    /**
     * 关键子查询
     */
    public void keyWordSearch(){
        String keyWord ="";

        ///条件搜索：餐厅名字(name和cnname)、菜系(cuisine)、地区（商圈area)、地址(address和cnaddress)
        MultiMatchQuery multiMatchQuery = QueryBuilders.multiMatch().fields("name", "cuisine", "area", "addrea").query(keyWord).build();
        BoolQuery boolQuery = QueryBuilders.bool().must(multiMatchQuery._toQuery()).build();

        NativeQuery build = NativeQuery.builder().withQuery(boolQuery._toQuery()).build();
        SearchHits<Book> search = elasticsearchTemplate.search(build, Book.class);
        List<Book> list = search.get().map(SearchHit::getContent).toList();



    }

//    /**
//     * 复杂查询
//     *
//     */
//    @Test
//    public void highlightStuDoc() {
//
//        //分页
//        Pageable pageable = PageRequest.of(0, 10);
//
//
//        String preTag = "<font color='red'>";
//        String postTag = "</font>";
//
//        SortBuilder sortBuilder = new FieldSortBuilder("money")
//                .order(SortOrder.DESC);
//        SortBuilder sortBuilderAge = new FieldSortBuilder("age")
//                .order(SortOrder.ASC);
//
//        Query query = new NativeSearchQueryBuilder()
//                .withQuery(QueryBuilders.matchQuery("description", "save man"))
//                .withHighlightFields(new HighlightBuilder.Field("description")
//                        .preTags(preTag)
//                        .postTags(postTag))
//                .withSort(sortBuilder)
//                .withSort(sortBuilderAge)
//                .withPageable(pageable)
//                .build();
//        AggregatedPage<Stu> pagedStu = esTemplate.queryForPage(query, Stu.class, new SearchResultMapper() {
//            @Override
//            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
//
//                List<Stu> stuListHighlight = new ArrayList<>();
//
//                SearchHits hits = response.getHits();
//                for (SearchHit h : hits) {
//                    HighlightField highlightField = h.getHighlightFields().get("description");
//                    String description = highlightField.getFragments()[0].toString();
//
//                    Object stuId = (Object)h.getSourceAsMap().get("stuId");
//                    String name = (String)h.getSourceAsMap().get("name");
//                    Integer age = (Integer)h.getSourceAsMap().get("age");
//                    String sign = (String)h.getSourceAsMap().get("sign");
//                    Object money = (Object)h.getSourceAsMap().get("money");
//
//                    Stu stuHL = new Stu();
//                    stuHL.setDescription(description);
//                    stuHL.setStuId(Long.valueOf(stuId.toString()));
//                    stuHL.setName(name);
//                    stuHL.setAge(age);
//                    stuHL.setSign(sign);
//                    stuHL.setMoney(Float.valueOf(money.toString()));
//
//                    stuListHighlight.add(stuHL);
//                }
//
//                if (stuListHighlight.size() > 0) {
//                    return new AggregatedPageImpl<>((List<T>)stuListHighlight);
//                }
//
//                return null;
//            }
//        });
//        System.out.println("检索后的总分页数目为：" + pagedStu.getTotalPages());
//        List<Stu> stuList = pagedStu.getContent();
//        for (Stu s : stuList) {
//            System.out.println(s);
//        }
//
//    }

}