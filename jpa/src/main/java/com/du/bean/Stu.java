package com.du.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author du
 */
@Document(indexName = "stu")
@Data
public class Stu {

    @Id
    private Long stuId;
    @Field(index = true, store = true)
    private String name;
    @Field(index = true, store = true)
    private Integer age;

    @Field
    private float money;

    @Field(store = true)
    private String desc;

    @Field(analyzer = "ik_max_word", store = true)
    private String description;

}
