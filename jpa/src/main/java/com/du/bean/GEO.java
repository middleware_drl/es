package com.du.bean;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "geo", createIndex = false)
@Data
public class GEO {

    private double lon;
    private double lat;

}
