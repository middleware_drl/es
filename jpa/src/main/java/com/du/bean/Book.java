package com.du.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author 杜瑞龙
 * &#064;date  2023/4/19 8:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName="book",createIndex=false)

public class Book {
    @Id
    private String id;

    @Field(type = FieldType.Text,analyzer = "icu_analyzer")
    private String name;

    @Field(type = FieldType.Text)
    private String summary;

    @Field(type = FieldType.Integer)
    private Integer price;
    @Field(type = FieldType.Date,pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;
}
