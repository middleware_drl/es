package com.du.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.GeoPointField;

/**
 * @author du
 */
@Document(indexName = "friends",  createIndex = false)
@Data
public class User {

    @Id
    private Long userId;

    @Field(store = true)
    private String userName;

    @Field(store = true)
    @GeoPointField
    private GEO geo;

    @Field(store = true)
    private String place;


}
