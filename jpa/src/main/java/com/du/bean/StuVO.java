package com.du.bean;

import lombok.Data;

/**
 * @author du
 */
@Data
public class StuVO {

    private Long stuId;
    private String name;
    private String description;


}
