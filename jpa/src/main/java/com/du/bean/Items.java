package com.du.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "foodie-items",  createIndex = false)
@Data
public class Items {

    @Id
    @Field(index = false, type = FieldType.Text)
    private String itemId;
    @Field(index = true, type = FieldType.Text)
    private String itemName;
    @Field(index = false, type = FieldType.Text)
    private String imgUrl;
    @Field(index = false, type = FieldType.Integer)
    private Integer price;
    @Field(index = false, type = FieldType.Integer)
    private Integer sellCounts;


}
