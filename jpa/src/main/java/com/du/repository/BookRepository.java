package com.du.repository;

import com.du.bean.Book;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2023/4/19 9:06
 */
@Repository
public interface BookRepository extends ListCrudRepository<Book, String>,PagingAndSortingRepository<Book,String> {
    /**
     * {
     *     "query": {
     *         "bool" : {
     *             "must" : [
     *                 { "query_string" : { "query" : "?", "fields" : [ "name" ] } },
     *                 { "query_string" : { "query" : "?", "fields" : [ "price" ] } }
     *             ]
     *         }
     *     }
     * }
     * @param name
     * @param price
     * @return
     */
    List<Book> findByNameAndPrice(String name, Integer price);
}
